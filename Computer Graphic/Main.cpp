#include <iostream>
#include <SDL.h>
#include "Bezier.h"
#include "FillColor.h"

using namespace std;

const int WIDTH  = 800;
const int HEIGHT = 1000;

SDL_Event event;


int main(int, char**){
	//First we need to start up SDL, and make sure it went ok
	if (SDL_Init(SDL_INIT_VIDEO) != 0){
		std::cout << "SDL_Init Error: " << SDL_GetError() << std::endl;
		return 1;
	}

	SDL_Window *win = SDL_CreateWindow("Hello World!", 0, 0, WIDTH, HEIGHT, SDL_WINDOW_SHOWN);
	//Make sure creating our window went ok
	if (win == NULL){
		std::cout << "SDL_CreateWindow Error: " << SDL_GetError() << std::endl;
		return 1;
	}

	//Create a renderer that will draw to the window, -1 specifies that we want to load whichever
    //DON'T FORGET CHANGE THIS LINE IN YOUR SOURCE ----->>>>> SDL_RENDERER_SOFTWARE

    SDL_Renderer *ren = SDL_CreateRenderer(win, -1, SDL_RENDERER_SOFTWARE);
	if (ren == NULL){
		SDL_DestroyWindow(win);
		std::cout << "SDL_CreateRenderer Error: " << SDL_GetError() << std::endl;
		SDL_Quit();
		return 1;
	}

    SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
	SDL_RenderClear(ren);

    //YOU CAN INSERT CODE FOR TESTING HERE

	Vector2D v1(100, 200);
	Vector2D v2(200, 400);
	Vector2D v3(250, 120);
	Vector2D v4(300, 300);

	SDL_Color fillColor;
	fillColor.a = 255;
	fillColor.b = 255;
	fillColor.g = 0;
	fillColor.r = 0;
	//TriangleFill(v1, v2, v3, ren, fillColor);

	SDL_Color boundaryfill;
	boundaryfill.a = 255;
	boundaryfill.b = 0;
	boundaryfill.g = 0;
	boundaryfill.r = 255;

	Uint32 pixel_format = SDL_GetWindowPixelFormat(win);
	SDL_SetRenderDrawColor(ren, 255, 0, 0, 255);
	Bresenham_Line(1, 2, 6, 6, ren);
	Bresenham_Line(4, 5, 6, 2, ren);
	Bresenham_Line(6, 2, 1, 2, ren);
	Vector2D t(3, 3);
	BoundaryFill4(win, t, pixel_format, ren, fillColor, boundaryfill);

	//FILL SHAPE

	//TriangleFill(v1, v2, v3, ren, fillColor);
	//RectangleFill(v1, v2, ren, fillColor);
	//CircleFill(100, 100, 50, ren, fillColor);

	//FILL INTERSECTION

	//FillIntersectionRectangleCircle(v1, v2, 150, 150, 100, ren, fillColor);
	//FillIntersectionEllipseCircle(250, 400, 200, 300, 400, 400, 200, ren, fillColor);
	//FillIntersectionTwoCircles(100, 100, 100, 150, 150, 100, ren, fillColor);
	
	//DRAW CURVE
	//DrawCurve2(ren, v1, v2, v3);
	//DrawCurve3(ren, v1, v2, v3, v4);

	//DrawCurve3(ren, v1, v2, v3, v4);
    SDL_RenderPresent(ren);
    //Take a quick break after all that hard work
    //Quit if happen QUIT event
	SDL_RenderPresent;
	bool running = true;
	float Da = 10, Db = 10, Dc = 10, Dd = 10;
	while(running)
	{
        //If there's events to handle
        if(SDL_PollEvent( &event))
        {
   //         //If the user has Xed out the window
   //         if( event.type == SDL_QUIT )
   //         {
   //             //Quit the program
   //             running = false;
   //         }
			//if (event.type == SDL_MOUSEBUTTONDOWN) {
			//	Vector2D temp;
			//	temp.x = event.button.x;
			//	temp.y = event.button.y;
			//	Da = sqrt((temp.x - v1.x)*(temp.x - v1.x) + (temp.y - v1.y)*(temp.y - v1.y));
			//	Db = sqrt((temp.x - v2.x)*(temp.x - v2.x) + (temp.y - v2.y)*(temp.y - v2.y));
			//	Dc = sqrt((temp.x - v3.x)*(temp.x - v3.x) + (temp.y - v3.y)*(temp.y - v3.y));
			//	Dd = sqrt((temp.x - v4.x)*(temp.x - v4.x) + (temp.y - v4.y)*(temp.y - v4.y));
			//}
			//if (event.type == SDL_MOUSEMOTION) {
			//	if (Da <= 5) {
			//		v1.x = event.button.x;
			//		v1.y = event.button.y;
			//		SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
			//		SDL_RenderClear(ren);
			//		DrawCurve3(ren, v1, v2, v3, v4);
			//		SDL_RenderPresent(ren);
			//	}
			//	if (Db <= 5) {
			//		v2.x = event.button.x;
			//		v2.y = event.button.y;
			//		SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
			//		SDL_RenderClear(ren);
			//		DrawCurve3(ren, v1, v2, v3, v4);
			//		SDL_RenderPresent(ren);
			//	}
			//	if (Dc <= 5) {
			//		v3.x = event.button.x;
			//		v3.y = event.button.y;
			//		SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
			//		SDL_RenderClear(ren);
			//		DrawCurve3(ren, v1, v2, v3, v4);
			//		SDL_RenderPresent(ren);
			//	}
			//	if (Dd <= 5) {
			//		v4.x = event.button.x;
			//		v4.y = event.button.y;
			//		SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
			//		SDL_RenderClear(ren);
			//		DrawCurve3(ren, v1, v2, v3, v4);
			//		SDL_RenderPresent(ren);
			//	}
			//}
			//if (event.type == SDL_MOUSEBUTTONUP) {
			//	Da = Db = Dc = Dd = 10;
			//}
        }

    }

	SDL_DestroyRenderer(ren);
	SDL_DestroyWindow(win);
	SDL_Quit();

	return 0;
}
