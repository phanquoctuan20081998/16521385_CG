#include "Parapol.h"

void Draw2Points(int xc, int yc, int x, int y, SDL_Renderer *ren)
{
    //draw 2 points
	SDL_RenderDrawPoint(ren, xc + x, yc + y);
	SDL_RenderDrawPoint(ren, xc - x, yc + y);
}
void BresenhamDrawParapolPositive(int xc, int yc, int A, SDL_Renderer *ren)
{
	int x = 0;
	int y  = 0;
	int p = 2 * x + 1 - A;
	Draw2Points(xc, yc, x, y, ren);
	while (x <= A) {
		if (p <= 0) {
			p += 2 * x + 3;
		}
		else {
			p += 2 * x + 3 - 2 * A;
			y++;
		}
		x++;
		Draw2Points(xc, yc, x, y, ren);
	}

	int w;
	int h;
	SDL_GetRendererOutputSize(ren, &w, &h);
	p = 4 * A - 2 * x - 1;
	while (y < h) {
		if (p <= 0) {
			p += 4 * A;
		}
		else {
			p += 4 * A - 4 * x;
			x++;
		}
		y++;
		Draw2Points(xc, yc, x, y, ren);
	}
}

void BresenhamDrawParapolNegative(int xc, int yc, int A, SDL_Renderer *ren)
{
	A = -A;
	int x = 0;
	int y = 0;
	int p = 2 * x + 1 + A;

	Draw2Points(xc, yc, x, y, ren);
	while (x <= -A) {
		if (p <= 0) {
			p += 2 * x + 3;
		}
		else {
			p += 2 * x + 3 + 2 * A;
			y--;
		}
		x++;
		Draw2Points(xc, yc, x, y, ren);
	}

	int w;
	int h;
	SDL_GetRendererOutputSize(ren, &w, &h);
	p = -4 * A - 2 * x - 1;
	while (y > -h) {
		if (p <= 0) {
			p += -4 * A;
		}
		else {
			p += -4 * A - 4 * (x + 1);
			x++;
		}    
		y--;
		Draw2Points(xc, yc, x, y, ren);
	}
}