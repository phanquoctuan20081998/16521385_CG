#include "Bezier.h"
#include "FillColor.h"
#include <iostream>
using namespace std;

void DrawCurve2(SDL_Renderer *ren, Vector2D p1, Vector2D p2, Vector2D p3)
{
	Vector2D temp;

	SDL_Color fillColor;
	fillColor.a = 255;
	fillColor.b = 0;
	fillColor.g = 255;
	fillColor.r = 0;

	//Draw 3 Bezier points

	CircleFill(p1.x, p1.y, 5, ren, fillColor);
	CircleFill(p2.x, p2.y, 5, ren, fillColor);
	CircleFill(p3.x, p3.y, 5, ren, fillColor);

	for (float t = 0; t < 1; t += 0.001) {
		temp.x = ((1 - t)*(1 - t))*p1.x + 2 * ((1 - t)*t)*p2.x + (t * t)*p3.x;
		temp.y = ((1 - t)*(1 - t))*p1.y + 2 * ((1 - t)*t)*p2.y + (t * t)*p3.y;
		SDL_RenderDrawPoint(ren, temp.x, temp.y);
	}
}
void DrawCurve3(SDL_Renderer *ren, Vector2D p1, Vector2D p2, Vector2D p3, Vector2D p4)
{
	Vector2D temp;

	SDL_Color fillColor;
	fillColor.a = 255;
	fillColor.b = 0;
	fillColor.g = 255;
	fillColor.r = 0;

	//Draw 4 Bezier points

	CircleFill(p1.x, p1.y, 5, ren, fillColor);
	CircleFill(p2.x, p2.y, 5, ren, fillColor);
	CircleFill(p3.x, p3.y, 5, ren, fillColor);
	CircleFill(p4.x, p4.y, 5, ren, fillColor);

	for (float t = 0; t < 1; t += 0.001) {
		temp.x = (((1 - t)*(1 - t))*(1 - t))*p1.x + 3 * (((1 - t)*(1 - t))*t)*p2.x + 3 * ((1 - t)*(t * t))*p3.x + (t*t)*(t*p4.x);
		temp.y = (((1 - t)*(1 - t))*(1 - t))*p1.y + 3 * (((1 - t)*(1 - t))*t)*p2.y + 3 * ((1 - t)*(t * t))*p3.y + (t*t)*(t*p4.y);
		SDL_RenderDrawPoint(ren, temp.x, temp.y);
	}
}


